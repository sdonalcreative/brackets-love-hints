define(function (require, exports, module) {
    'use strict';
    
    // Extension Modules
    var InlineDocsViewer = require("InlineDocsViewer");
    
    var currentTokenMatcher = /love((?:\.[a-zA-Z][a-zA-Z0-9_]*|\.)+)$/g;
    var tokenSplitter = /([a-zA-Z][a-zA-Z0-9_]*)(?:$|\.)/g;
    var loveWikiPrefix = 'https://love2d.org/wiki/';
    
    function getTokensFromSelection(editor) {
        var subTokens = [];

        var cursor = editor.getSelection();
        var lineBeginning = { line:cursor.start.line, ch:0 };
        var textBeforeCursor = editor.document.getRange(lineBeginning, cursor.end);

        var token = currentTokenMatcher.exec(textBeforeCursor);
        if (token) {
            var subToken = token[1];
            var currentSubToken;
            while ((currentSubToken = tokenSplitter.exec(subToken))) {
                subTokens.push(currentSubToken[1]);
            }
        }
        currentTokenMatcher.lastIndex = 0;

        return token ? subTokens : null;
    }
    
    function getLoveThingFromTokens(documentation, tokens) {
        var currentThing = documentation;
        var currentType = '';
        var grepResult, currentToken;
        
        var thingGrep = function(thing){ return thing.name == currentToken; };
        
        for (var i = 0; i < tokens.length; i++) {
            currentToken = tokens[i];
            
            if (currentThing.modules) {
                grepResult = $.grep(currentThing.modules, thingGrep);
                if (grepResult.length == 1) {
                    currentThing = grepResult[0];
                    currentType = 'module';
                    continue;
                }
            }
            
            if (currentThing.functions) {
                grepResult = $.grep(currentThing.functions, thingGrep);
                if (grepResult.length == 1) {
                    currentThing = grepResult[0];
                    currentType = 'function';
                    continue;
                }
            }
            
            if (currentThing.callbacks) {
                grepResult = $.grep(currentThing.callbacks, thingGrep);
                if (grepResult.length == 1) {
                    currentThing = grepResult[0];
                    currentType = 'callback';
                    continue;
                }
            }
            
            return null; // Token didn't match anything. No docs available!
        }
        
        return { docs: currentThing, type: currentType };
    }
    
    function summarize(str) {
        var result = /^.*?[\.!\?](?:\s|$)/.exec(str);
        return result ? result[0] : str;
    }
    
    function paragraphize(description) {
        description = description.replace(/\n{2}/g, '</p><p>');
        description = description.replace(/\n/g, '<br />');
        return '<p>' + description + '</p>';
    }
    
    function getFunctionArgumentsString(loveFunction, variantIndex) {
        variantIndex = variantIndex | 0;
        var variant = loveFunction.variants[variantIndex];
        
        if (!variant) {
            return '';
        }
        
        if (!variant.arguments) {
            return '()';
        }
        
        var variantArguments = [];
        for (var i = 0; i < variant.arguments.length; i++) {
            var currentArgument = variant.arguments[i];
            variantArguments.push('<small>' + currentArgument.type + '</small> ' + currentArgument.name);
        }
        
        return '(' + variantArguments.join(', ') + ')';
    }
    
    function getDocsValuesForModule(loveModule, moduleName) {
        var values = [];
        
        for (var i = 0; i < loveModule.functions.length; i++) {
            var currentFunction = loveModule.functions[i];
            
            var functionName = moduleName + '.' + currentFunction.name;
            var functionArguments = getFunctionArgumentsString(currentFunction);
            var variantsText = '';
            if (currentFunction.variants.length > 1) {
                var numOthers = currentFunction.variants.length - 1;
                variantsText = '<small>+' + numOthers + ' other ';
                variantsText += numOthers > 1 ? 'variants' : 'variant';
                variantsText += '</small>';
            }
            
            values.push({
                value: '<code><a href="' + loveWikiPrefix + functionName + '">' + functionName + '</a>' + functionArguments + '</code> ' + variantsText,
                description: paragraphize(summarize(currentFunction.description))
            });
        }
        
        return values;
    }
    
    function getDocsValuesForFunction(loveFunction, functionName) {
        var values = [];
        
        for (var i = 0; i < loveFunction.variants.length; i++) {
            var currentVariant = loveFunction.variants[i];
            var argumentsString = getFunctionArgumentsString(loveFunction, i);
            var descriptionString = '';
            
            if (currentVariant.arguments) {
                var argumentStrings = [];
                for (var o = 0; o < currentVariant.arguments.length; o++) {
                    var currentArgument = currentVariant.arguments[o];
                    
                    argumentStrings.push('<dt><code>' + currentArgument.name + ' <small><em>' + currentArgument.type + '</em></small></code></dt><dd>' + paragraphize(currentArgument.description) + '</dd>');
                }
                
                descriptionString = '<dl>' + argumentStrings.join('') + '</dl>';
            }
            
            values.push({
                value: '<code>' + functionName + argumentsString + '</code> ',
                description: descriptionString
            });
        }
        
        return values;
    }
    
    function getInlineDocs(loveTokens, loveThing) {
        var name = 'love.' + loveTokens.join('.');
        var thingDocs = { SUMMARY: paragraphize(loveThing.docs.description), URL: loveWikiPrefix + name };
        
        switch(loveThing.type) {
            case 'module':
                thingDocs.VALUES = getDocsValuesForModule(loveThing.docs, name);
                return thingDocs;
            case 'function':
            case 'callback':
                thingDocs.VALUES = getDocsValuesForFunction(loveThing.docs, name);
                return thingDocs;
        }
    }
    
    function LoveDocs(documentation) {
        /**
         * @param {!Editor} editor
         * @param {!{line:Number, ch:Numbpos
         * @return {?$.Promise} resolved with an InlineWidget; null if we're not going to provide anything
         */
        return function(hostEditor, pos) {
            var langId = hostEditor.getLanguageForSelection().getId();
            if (langId !== "lua") {
                return null;
            }
            
            var loveTokens = getTokensFromSelection(hostEditor);
            if (!loveTokens) {
                return null;
            }

            var loveThing = getLoveThingFromTokens(documentation, loveTokens);
            if (!loveThing) {
                return null;
            }
            
            var result = new $.Deferred();
            var inlineWidget = new InlineDocsViewer('love.' + loveTokens.join('.'), getInlineDocs(loveTokens, loveThing));
            inlineWidget.load(hostEditor);
            result.resolve(inlineWidget);
            return result.promise();
        };
    }
    
    module.exports = LoveDocs;
});