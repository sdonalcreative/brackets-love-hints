define(function (require, exports, module) {
    'use strict';

    function LoveHints(documentation) {
        this.currentTokenMatcher = /love((?:\.[a-zA-Z][a-zA-Z0-9_]*|\.)+)$/g;
        this.tokenSplitter = /([a-zA-Z][a-zA-Z0-9_]*)(?:$|\.)/g;
        this.documentation = documentation;
    }

    LoveHints.prototype.getTokensFromCursor = function (editor) {
        var subTokens = [];

        this.editor = editor;
        var cursor = this.editor.getCursorPos();
        var lineBeginning = {line:cursor.line,ch:0};
        var textBeforeCursor = this.editor.document.getRange(lineBeginning, cursor);

        var token = this.currentTokenMatcher.exec(textBeforeCursor);
        if (token) {
            var subToken = token[1];
            var currentSubToken;
            while ((currentSubToken = this.tokenSplitter.exec(subToken))) {
                subTokens.push(currentSubToken[1]);
            }
        }
        this.currentTokenMatcher.lastIndex = 0;

        return token ? subTokens : null;
    };

    LoveHints.prototype.getHasHints = function (tokens) {
        if (tokens === null) return false;
        var token;
        var functionIndex;
        var currentFunction;
        var foundModule = null;

        if (tokens.length === 0) return true; // Show all the hints.
        if (tokens.length >= 1) { // Top level token search
            token = tokens[0];
            for (var moduleIndex = 0; moduleIndex < this.documentation.modules.length; moduleIndex++) {
                var currentModule = this.documentation.modules[moduleIndex];
                if (currentModule.name.startsWith(token)) {
                    foundModule = currentModule;
                    break;
                }
            }

            for (var callbackIndex = 0; callbackIndex < this.documentation.callbacks.length; callbackIndex++) {
                var callback = this.documentation.callbacks[callbackIndex];
                if (callback.name.startsWith(token)) return true;
            }

            for (functionIndex = 0; functionIndex < this.documentation.functions.length; functionIndex++) {
                currentFunction = this.documentation.functions[functionIndex];
                if (currentFunction.name.startsWith(token)) return true;
            }
        }
        if (tokens.length === 1 && foundModule) return true;
        if (tokens.length === 2 && foundModule) { // Second level search
            token = tokens[1];

            for (functionIndex = 0; functionIndex < foundModule.functions.length; functionIndex++) {
                currentFunction = foundModule.functions[functionIndex];
                if (currentFunction.name.startsWith(token)) return true;
            }
        }

        return false;
    };

    LoveHints.prototype.hasHints = function (editor, implicitChar) {
        return this.getHasHints(this.getTokensFromCursor(editor));
    };

    LoveHints.prototype.getHints = function (implicitChar) {
        var tokens = this.getTokensFromCursor(this.editor);
        if (tokens === null) return null;

        var hints = [];
        var token;
        var functionIndex, currentFunction;
        var moduleIndex, currentModule;
        var callbackIndex, callback;
        var foundModule = null;

        if (tokens.length === 0) {
            for (functionIndex = 0; functionIndex < this.documentation.functions.length; functionIndex++) {
                currentFunction = this.documentation.functions[functionIndex];
                hints.push('love.' + currentFunction.name);
            }

            for (callbackIndex = 0; callbackIndex < this.documentation.callbacks.length; callbackIndex++) {
                callback = this.documentation.callbacks[callbackIndex];
                hints.push('love.' + callback.name);
            }

            for (moduleIndex = 0; moduleIndex < this.documentation.modules.length; moduleIndex++) {
                currentModule = this.documentation.modules[moduleIndex];
                hints.push('love.' + currentModule.name);
            }
        }
        if (tokens.length >= 1) { // Top level token search
            token = tokens[0];
            for (moduleIndex = 0; moduleIndex < this.documentation.modules.length; moduleIndex++) {
                currentModule = this.documentation.modules[moduleIndex];
                if (currentModule.name === token) {
                    foundModule = currentModule;
                    break;
                }
            }
        }
        if (tokens.length === 1 && !foundModule) {
            token = tokens[0];

            for (functionIndex = 0; functionIndex < this.documentation.functions.length; functionIndex++) {
                currentFunction = this.documentation.functions[functionIndex];
                if (currentFunction.name.startsWith(token) && currentFunction.name != token) hints.push('love.' + currentFunction.name);
            }

            for (callbackIndex = 0; callbackIndex < this.documentation.callbacks.length; callbackIndex++) {
                callback = this.documentation.callbacks[callbackIndex];
                if (callback.name.startsWith(token) && callback.name != token) hints.push('love.' + callback.name);
            }

            for (moduleIndex = 0; moduleIndex < this.documentation.modules.length; moduleIndex++) {
                currentModule = this.documentation.modules[moduleIndex];
                if (currentModule.name.startsWith(token) && currentModule.name != token) hints.push('love.' + currentModule.name);
            }
        }
        if (tokens.length === 1 && foundModule) {
            for (functionIndex = 0; functionIndex < foundModule.functions.length; functionIndex++) {
                currentFunction = foundModule.functions[functionIndex];
                hints.push('love.' + foundModule.name + '.' + currentFunction.name);
            }
        }
        if (tokens.length === 2 && foundModule) { // Second level search
            token = tokens[1];

            for (functionIndex = 0; functionIndex < foundModule.functions.length; functionIndex++) {
                currentFunction = foundModule.functions[functionIndex];
                if (currentFunction.name.startsWith(token) && currentFunction.name != token) hints.push('love.' + foundModule.name + '.' + currentFunction.name);
            }
        }

        return {
            hints: hints,
            match: 'love.' + tokens.join('.'),
            selectInitial: true,
            handleWideResults: false
        };
    };

    LoveHints.prototype.insertHint = function (hint) {
        var cursor = this.editor.getCursorPos();
        var lineBeginning = { line:cursor.line, ch:0 };
        var textBeforeCursor = this.editor.document.getRange(lineBeginning, cursor);
        var indexOfTheSymbol = textBeforeCursor.search(this.currentTokenMatcher);
        var replaceStart = { line:cursor.line, ch:indexOfTheSymbol};
        this.editor.document.replaceRange(hint, replaceStart, cursor);

        return true;
    };

    module.exports = LoveHints;
});