# LÖVE Hints for Brackets.io

![Screenshot of LÖVE Hints' code hinting](http://i.imgur.com/zsCtk7G.png)
![Screenshot 1 of LÖVE Hints' quick documentation](http://i.imgur.com/0W118pF.png)
![Screenshot 2 of LÖVE Hints' quick documentation](http://i.imgur.com/w4j2EhB.png)

This extension provides code hinting and "Quick Docs" for Love2D, the fantastic game engine that's open source, and multi-platform.

## Coming Soon
- More Quick Docs features, for extended support.

## Acknowledgements
Thank you to the wonderful contributors working on [LOVE-API](https://github.com/rm-code/love-api), without your efforts, this wouldn't be possible.