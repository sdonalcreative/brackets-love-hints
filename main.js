/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50 */
/*global define, $, brackets, window */

define(function (require, exports, module) {
    'use strict';
    
    // Core Modules
    var AppInit = brackets.getModule('utils/AppInit');
    var CodeHintManager = brackets.getModule('editor/CodeHintManager');
    var EditorManager = brackets.getModule("editor/EditorManager");
    
    // Data
    var loveDocumentation = JSON.parse(require('text!love2d.json'));
    
    // Extension Modules
    require('string'); // Adds startsWith function.
    var LoveHints = require('LoveHints');
    var LoveDocs = require('LoveDocs')(loveDocumentation);
    
    AppInit.appReady(function () {
        var loveHints = new LoveHints(loveDocumentation);
        CodeHintManager.registerHintProvider(loveHints, ['lua'], 10);
        
        EditorManager.registerInlineDocsProvider(LoveDocs);
    });
});